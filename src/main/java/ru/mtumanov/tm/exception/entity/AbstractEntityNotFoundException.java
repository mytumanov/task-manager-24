package ru.mtumanov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    protected AbstractEntityNotFoundException() {
    }

    protected AbstractEntityNotFoundException(@NotNull final String message) {
        super(message);
    }

    protected AbstractEntityNotFoundException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    protected AbstractEntityNotFoundException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractEntityNotFoundException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
