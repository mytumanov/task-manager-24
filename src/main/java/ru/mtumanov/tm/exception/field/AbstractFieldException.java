package ru.mtumanov.tm.exception.field;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    protected AbstractFieldException() {
    }

    protected AbstractFieldException(@NotNull final String message) {
        super(message);
    }

    protected AbstractFieldException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    protected AbstractFieldException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractFieldException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
