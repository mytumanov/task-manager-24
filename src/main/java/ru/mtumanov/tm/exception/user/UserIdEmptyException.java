package ru.mtumanov.tm.exception.user;

public class UserIdEmptyException extends AbstractUserException {

    public UserIdEmptyException() {
        super("ERROR! User id is empty!");
    }

}
