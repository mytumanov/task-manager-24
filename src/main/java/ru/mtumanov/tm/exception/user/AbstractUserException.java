package ru.mtumanov.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    protected AbstractUserException() {
    }

    protected AbstractUserException(@NotNull final String message) {
        super(message);
    }

    protected AbstractUserException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    protected AbstractUserException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractUserException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
