package ru.mtumanov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.model.IHaveCreated;

import java.util.Comparator;

public enum CreatedComparator implements Comparator<IHaveCreated> {

    INSTANCE;

    @Override
    public int compare(@Nullable final IHaveCreated o1, @Nullable final IHaveCreated o2) {
        if (o1 == null || o2 == null)
            return 0;
        if (o1.getCreated() == null || o2.getCreated() == null)
            return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
