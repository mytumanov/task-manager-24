package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.IUserOwnedRepository;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> modelList = findAll(userId);
        models.removeAll(modelList);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        try {
            findOneById(userId, id);
            return true;
        } catch (AbstractEntityNotFoundException e) {
            return false;
        }
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id)
            throws AbstractEntityNotFoundException {
        return models.stream()
                .filter(m -> id.equals(m.getId()) && userId.equals(m.getUserId()))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return (int) models.stream().filter(m -> userId.equals(m.getUserId())).count();
    }

    @Override
    @NotNull
    public M remove(@NotNull final String userId, @NotNull final M model) throws AbstractEntityNotFoundException {
        return removeById(userId, model.getId());
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String userId, @NotNull final String id) throws AbstractEntityNotFoundException {
        @NotNull final M model = findOneById(userId, id);
        return remove(model);
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final M model = findOneByIndex(userId, index);
        return remove(model);
    }

}
